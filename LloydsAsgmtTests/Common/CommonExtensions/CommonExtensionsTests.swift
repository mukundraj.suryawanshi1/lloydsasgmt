//
//  CommonExtensionsTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt

final class CommonExtensionsTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }

    func testApiInvalidJsonData() {
        let apiError = APIError.invalidJsonData
        XCTAssertEqual(apiError.errorDescription, APIError.invalidJsonData.errorDescription)
    }
    
    func testApiInvalidJsonResponse() {
        let apiError = APIError.invalidJsonResponse
        XCTAssertEqual(apiError.errorDescription, APIError.invalidJsonResponse.errorDescription)
    }
    
    func testApiInvalidJsonUrl() {
        let apiError = APIError.invalidUrl
        XCTAssertEqual(apiError.errorDescription, APIError.invalidUrl.errorDescription)
    }
    
    func testApiInvalidJsonParsingError() {
        let error = "Something went wrong..401"
        let apiError = APIError.jsonParsingError("401")
        XCTAssertEqual(apiError.errorDescription, error)
    }
    
    func testImageEmptyPathUrl() {
        let imgPath = String().imagePath(imgUrl: nil)
        XCTAssertTrue(imgPath.isEmpty)
    }
    func testImagePathUrl() {
        let imgPath = String().imagePath(imgUrl: NekoUtilityManager.nekoArtistDomainModel().url?.absoluteString)
        XCTAssertNotNil(imgPath)
    }

}
