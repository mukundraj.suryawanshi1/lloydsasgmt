//
//  APPConstants.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 07/02/24.
//

import XCTest
import SwiftUI
@testable import LloydsAsgmt
final class APPConstantsTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testAppNavTitle() {
        let title = "Nekobeset"
        XCTAssertEqual(title, NavTitleConstants.homeNavTitle.description)
        
        let titleTxt = "Neko"
        XCTAssertNotEqual(titleTxt, NavTitleConstants.homeNavTitle.description)

    }
    
    func testButtonTitle() {
        let buttonTitle = "Alert!"
        XCTAssertEqual(buttonTitle, NavTitleConstants.alertButtonTitle.description)
        
        let buttonTitleTxt = "Cancel"
        XCTAssertNotEqual(buttonTitleTxt, NavTitleConstants.alertButtonTitle.description)

    }
    
    func testImagePlaceholder() {
        let placeholderImg = "placeholder"
        XCTAssertEqual(placeholderImg, ImagePlaceHolder.defaultImage.description)
        
        let placeholderImage = "default"
        XCTAssertNotEqual(placeholderImage, ImagePlaceHolder.defaultImage.description)
    }
    
    func testBackgroundColor() {
        let bgColors = Color.teal
        XCTAssertEqual(bgColors, Colors.backgroundViewColor.bgColor)
        
        let bgColor = Color.red
        XCTAssertNotEqual(bgColor, Colors.backgroundViewColor.bgColor)
    }
    
    func testNavigationAscentColor() {
        let bgColors = Color.white
        XCTAssertEqual(bgColors, Colors.navigationAccentColor.bgColor)
        
        let bgColor = Color.red
        XCTAssertNotEqual(bgColor, Colors.navigationAccentColor.bgColor)
    }
    
    func testHomeImageSize(){
        let imgSize = CGSize(width: 180, height: 180)
        XCTAssertEqual(ImageSize.nekoImageSize(viewType: .HomeContentView).width, imgSize.width)
        XCTAssertEqual(ImageSize.nekoImageSize(viewType: .HomeContentView).height, imgSize.height)
        
        let imgDiffSize = CGSize(width: 120, height: 120)
        XCTAssertNotEqual(ImageSize.nekoImageSize(viewType: .HomeContentView).width, imgDiffSize.width)
        XCTAssertNotEqual(ImageSize.nekoImageSize(viewType: .HomeContentView).height, imgDiffSize.height)

    }
    
    func testDetailsImageSize(){
        let imgSize = CGSize(width: 350, height: 350)
        XCTAssertEqual(ImageSize.nekoImageSize(viewType: .DetailsListContentView).width, imgSize.width)
        XCTAssertEqual(ImageSize.nekoImageSize(viewType: .DetailsListContentView).height, imgSize.height)
        
        let imgDiffSize = CGSize(width: 450, height: 450)
        XCTAssertNotEqual(ImageSize.nekoImageSize(viewType: .DetailsListContentView).width, imgDiffSize.width)
        XCTAssertNotEqual(ImageSize.nekoImageSize(viewType: .DetailsListContentView).height, imgDiffSize.height)
    }
    
    func testPaddingSize(){
        let vstackSpacing = 20.0
        let cornerRadius = 18.0
        let topPadding = 100.0
        XCTAssertEqual(vstackSpacing, DetailsViewSize.VStackSpacing.values)
        XCTAssertEqual(cornerRadius, DetailsViewSize.ImageCornerRadius.values)
        XCTAssertEqual(topPadding, DetailsViewSize.topPadding.values)
        
        let vstackDiffSpacing = 30.0
        let cornerDiffRadius = 38.0
        let topDiffPadding = 500.0
        XCTAssertNotEqual(vstackDiffSpacing, DetailsViewSize.VStackSpacing.values)
        XCTAssertNotEqual(cornerDiffRadius, DetailsViewSize.ImageCornerRadius.values)
        XCTAssertNotEqual(topDiffPadding, DetailsViewSize.topPadding.values)
    }
    
}
