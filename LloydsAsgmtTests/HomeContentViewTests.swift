//
//  HomeContentViewTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class HomeContentViewTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }

    func testListEmptyData(){
        let emptyData = NekobesetListDataModel(results: [])
        XCTAssertTrue(emptyData.results.isEmpty)
    }
    
    func testlistData(){
        let listData = NekobesetListDomainModel(results: [ArtistDomainModel(artistHref: "https://twitter.com/Aramarufox/", artistName: "Aramarufox", sourceURL: URL(string: "https://twitter.com/Aramarufox/status/1491744680999940101"), url: URL(string: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png"))])
        XCTAssertNotNil(listData)
    }
    
    func testInvalidUrl(){
        testlistData()
    }
    
    func testListDataModelData(){
        let listData = NekobesetListDataModel(results: [ArtistDataModel(artistHref: "https://twitter.com/Aramarufox/", artistName: "Aramarufox", sourceURL: "https://twitter.com/Aramarufox/status/1491744680999940101", url: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png")])
        XCTAssertNotNil(listData)
    }
    func testMockListData(){
        let listData = NekobesetListDataModel(results: [ArtistDataModel(artistHref: "https://twitter.com/Aramarufox/", artistName: "Aramarufox", sourceURL: "https://twitter.com/Aramarufox/status/1491744680999940101", url: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png")])
        XCTAssertEqual(listData.results[0].artistName, MockData().samples.artistName)
        XCTAssertEqual(listData.results[0].artistHref, MockData().samples.artistHref)
        XCTAssertEqual(URL(string: listData.results[0].url), MockData().samples.url)
        XCTAssertEqual(URL(string: listData.results[0].sourceURL), MockData().samples.sourceURL)
    }
    func testMockListPresentationData(){
        let listData = HomePresentationModel(results: [ArtistPresentationModel(artistHref: "https://twitter.com/Aramarufox/", artistName: "Aramarufox", sourceURL: URL(string: "https://twitter.com/Aramarufox/status/1491744680999940101"), url: URL(string: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png"))])
        XCTAssertEqual(listData.results[0].artistName, MockPresentationData().samples.artistName)
        XCTAssertEqual(listData.results[0].artistHref, MockPresentationData().samples.artistHref)
        XCTAssertNotEqual(listData.results[0].url!, MockPresentationData().samples.url!)
        XCTAssertEqual(listData.results[0].sourceURL!, MockPresentationData().samples.sourceURL!)
    }
}
