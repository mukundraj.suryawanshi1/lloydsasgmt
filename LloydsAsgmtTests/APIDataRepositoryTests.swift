//
//  APIDataRepositoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class APIDataRepositoryTests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
    }
   
    func testRepositoryApiMethod(){
        let repoFactory = RepositoryFactory.createRepository()
        repoFactory.makeApiCall { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                XCTAssert(!model.results.isEmpty)
            case .failure(let error):
                XCTAssert(!error.localizedDescription.isEmpty)
            }
        }
    }
}
