//
//  NekobsetListMapperTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 31/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class NekobsetListMapperTests: XCTestCase {
    
    let dataModel = NekoUtilityManager.nekoListDataModel()
    
    override class func setUp() {
        super.setUp()
    }
    
    func testMapperDataToDomainModel() {
        let mapper = NekobsetListMapper()
        let results = mapper.fromDataToDomainModel(dataModel: dataModel)
        XCTAssertNotNil(results)
        XCTAssertEqual(results?.results[0].artistHref, "https://twitter.com/Aramarufox/")
        XCTAssertEqual(results?.results[0].artistName, "Aramarufox")
        XCTAssertEqual(results?.results[0].sourceURL?.absoluteString, "https://twitter.com/Aramarufox/status/1491744680999940101")
        XCTAssertEqual(results?.results[0].url?.absoluteString, "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png")
        
        XCTAssertEqual(results?.results[1].artistHref, "https://www.pixiv.net/en/users/12501110")
        XCTAssertEqual(results?.results[1].artistName, "maruma")
        XCTAssertEqual(results?.results[1].sourceURL?.absoluteString, "https://www.pixiv.net/en/artworks/78293758")
        XCTAssertEqual(results?.results[1].url?.absoluteString, "https://nekos.best/api/v2/neko/dd9e714a-898d-4a36-bdf1-633303706f04.png")
  
        XCTAssertEqual(results?.results[2].artistHref, "https://www.pixiv.net/en/users/12501110")
        XCTAssertEqual(results?.results[2].artistName, "maruma")
        XCTAssertEqual(results?.results[2].sourceURL?.absoluteString, "https://www.pixiv.net/en/artworks/78293758")
        XCTAssertEqual(results?.results[2].url?.absoluteString, "https://nekos.best/api/v2/neko/9fd4bc89-a093-4087-b9a6-59ff79d978f6.png")
        
        XCTAssertEqual(results?.results[3].artistHref, "https://www.pixiv.net/en/users/12501110")
        XCTAssertEqual(results?.results[3].artistName, "maruma")
        XCTAssertEqual(results?.results[3].sourceURL?.absoluteString, "https://www.pixiv.net/en/artworks/82938575")
        XCTAssertEqual(results?.results[3].url?.absoluteString, "https://nekos.best/api/v2/neko/965cf4ee-dc04-45a5-afe7-cb2609f6a2f6.png")
  
        XCTAssertEqual(results?.results[4].artistHref, "https://www.pixiv.net/en/users/24157148")
        XCTAssertEqual(results?.results[4].artistName, "srm")
        XCTAssertEqual(results?.results[4].sourceURL?.absoluteString, "https://www.pixiv.net/en/artworks/99899553")
        XCTAssertEqual(results?.results[4].url?.absoluteString, "https://nekos.best/api/v2/husbando/d18cb369-9a07-45aa-b213-e8618cb5bc86.png")
    }
    
    func testInvalidMapperDataToDomainModel() {
        let mapper = NekobsetListMapper()
        let results = mapper.fromDataToDomainModel(dataModel: dataModel)
        XCTAssertNotNil(results)
        XCTAssertNotEqual(results?.results[0].artistHref, nil)
        XCTAssertNotEqual(results?.results[0].artistName, nil)
        XCTAssertNotEqual(results?.results[0].sourceURL, nil)
        XCTAssertNotEqual(results?.results[0].url, nil)
    }
    
    func testEmptyMapperDataToDomainModel() {
        let mapper = NekobsetListMapper()
        let resModel = mapper.fromDataToDomainModel(dataModel: NekobesetListDataModel(results: []))
        XCTAssertTrue((resModel?.results.count) == 0)
    }
    
}
