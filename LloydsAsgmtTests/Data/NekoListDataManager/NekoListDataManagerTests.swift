//
//  APIDataRepositoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class NekoListDataManagerTests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
    }
   
    func testApiDataManager() {
        let dataManager = NekoListDataManager(apiServiceManager: APIServiceManager(apiService: APIService()))
        dataManager.fetchNekoList { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                XCTAssertTrue(!model.results.isEmpty)
            case .failure(let error):
                XCTAssertTrue(!error.localizedDescription.isEmpty)
            }
        }
    }
}
