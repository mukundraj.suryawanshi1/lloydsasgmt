//
//  APIConstantsTest.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 31/01/24.
//

import XCTest
@testable import LloydsAsgmt
import SwiftUI

final class APIConstantsTest: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testApiConstants() {
        let baseUrl = "nekos.best"
        XCTAssertEqual(baseUrl, APIConstants.baseURL.description)
        
        let middlePath = "/api/v2/search?query=Aramaru&type=1&amount=100"
        XCTAssertEqual(middlePath, APIConstants.fetchNekoListUrl.description)
    }
    
    func testNavTitleConstants() {
        let navTitle = "Nekobeset"
        XCTAssertEqual(navTitle, NavTitleConstants.homeNavTitle.description)
    }

    func testImagePlacholderConstants() {
        let placeHolderImg = "placeholder"
        XCTAssertEqual(placeHolderImg, ImagePlaceHolder.defaultImage.description)
    }
    
    func testBackgroundColorConstants() {
        let bgColor = Color.teal
        XCTAssertEqual(bgColor, Colors.backgroundViewColor.bgColor)
    }
    
    func testInvalidApiConstants() {
        let baseUrl = "nekos.best.com"
        XCTAssertNotEqual(baseUrl, APIConstants.baseURL.description)
        
        let middlePath = "/api/v2/search?query=Aramaru&type=1&amount=1000000"
        XCTAssertNotEqual(middlePath, APIConstants.fetchNekoListUrl.description)
    }
    
    func testInvalidNavTitleConstants() {
        let navTitle = "Nekobeset list"
        XCTAssertNotEqual(navTitle, NavTitleConstants.homeNavTitle.description)
    }

    func testInvalidImagePlacholderConstants() {
        let placeHolderImg = "placeholder image"
        XCTAssertNotEqual(placeHolderImg, ImagePlaceHolder.defaultImage.description)
    }
    
    func testInvalidBackgroundColorConstants() {
        let bgColor = Color.red
        XCTAssertNotEqual(bgColor, Colors.backgroundViewColor.bgColor)
    }
    
}
