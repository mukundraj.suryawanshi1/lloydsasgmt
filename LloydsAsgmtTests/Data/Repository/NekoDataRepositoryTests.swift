//
//  NekoDataRepositoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt

final class NekoDataRepositoryTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testRepo() {
        let repo = NekoListDataRepository(service: NekoListDataManager(apiServiceManager: APIServiceManagerFactory.createManager()), mapper: NekobsetListMapper())
        XCTAssertNotNil(repo)
    }
    
    func testRepoFactory() {
        let repoFactory = NekoListDataRepositoryFactory.createRepository()
        repoFactory.makeApiCall { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                XCTAssertTrue(!model.results.isEmpty)
            case .failure(let error):
                XCTAssertTrue(!error.localizedDescription.isEmpty)
            }
        }
    }
}
