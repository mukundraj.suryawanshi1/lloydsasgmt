//
//  APIServicesManagerTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import SwiftUI
import XCTest
@testable import LloydsAsgmt

final class APIServicesManagerTests: XCTestCase {
    override class func setUp() {
        super.setUp()
    }
    
    func testApiServiceManager() {
        let apifactory = APIServiceManagerFactory.createManager()
        apifactory.fetchData(.fetchNekoList, model: NekobesetListDataModel.self) { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                XCTAssertNotNil(model)
            case .failure(let error):
                XCTAssertNotNil(error.localizedDescription)
            }
        }
    }
    
    func testApiService() {
        let apiServiceFactory = APIServiceHelperFactory.createService()
        apiServiceFactory.fetchNekoList { jsonHandler in
            XCTAssertNotNil(jsonHandler)
        }
    }
}
