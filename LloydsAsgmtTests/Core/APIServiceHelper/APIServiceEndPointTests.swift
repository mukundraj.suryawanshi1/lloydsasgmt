//
//  APIServiceEndPointTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 31/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class APIServiceEndPointTests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
    }
    
    func testEndpoints() {
        let scheme = "https"
        XCTAssertEqual(scheme, APIServiceEndPoint.fetchNekoList.scheme)
        
        let baseUrl = "nekos.best"
        XCTAssertEqual(baseUrl, APIServiceEndPoint.fetchNekoList.baseURL)
        
        
        let path = "/api/v2/search?query=Aramaru&type=1&amount=100"
        XCTAssertEqual(path, APIServiceEndPoint.fetchNekoList.path)
        
        let methodType = HTTPMethod.get
        XCTAssertEqual(methodType, APIServiceEndPoint.fetchNekoList.httpMethod)
        
        let data = Data()
        XCTAssertNotEqual(data, APIServiceEndPoint.fetchNekoList.data)
        
    }
    
    func testInvalidEndpoints() {
        let scheme = "http"
        XCTAssertNotEqual(scheme, APIServiceEndPoint.fetchNekoList.scheme)
        
        let baseUrl = "nekos1.best.com"
        XCTAssertNotEqual(baseUrl, APIServiceEndPoint.fetchNekoList.baseURL)
        
        
        let path = "/api/v2/search?query=Aramaru&type=1&amount=100000"
        XCTAssertNotEqual(path, APIServiceEndPoint.fetchNekoList.path)
        
        let methodType = HTTPMethod.post
        XCTAssertNotEqual(methodType, APIServiceEndPoint.fetchNekoList.httpMethod)
        
        let data = Data()
        XCTAssertNotEqual(data, APIServiceEndPoint.fetchNekoList.data)
        
    }
    
}
