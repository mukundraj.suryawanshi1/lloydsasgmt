//
//  APIServiceHelperFactoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt
final class APIServiceHelperFactoryTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testApiServiceHelperFactory() {
        let apiServiceFactory = APIServiceHelperFactory.createService()
        apiServiceFactory.fetchNekoList { jsonHandler in
            XCTAssertNotNil(jsonHandler)
        }
    }
}
