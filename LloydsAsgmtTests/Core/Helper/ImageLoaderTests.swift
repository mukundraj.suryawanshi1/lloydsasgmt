//
//  ImageLoaderTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 31/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class ImageLoaderTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testImageLoaderView() {
        let cv = NekoRemoteImageView(url: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png", imageWidth: 200, imageHeight: 200)
        let body = cv.body
        XCTAssertNotNil(body)
    }
    
    func testRemoteImage() {
        let apiService = APIService<APIServiceEndPoint>()
        apiService.imageDownload(url: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png") { imgData in
            XCTAssertNotNil(imgData)
        }
        
        apiService.imageDownload(url: "") { imgData in
            XCTAssertNil(imgData)
        }
    }
    
    func testInvalidRemoteImage() {
        let apiService = APIService<APIServiceEndPoint>()
        apiService.imageDownload(url: "www.google.com") { imgData in
            XCTAssertNil(imgData)
        }
    }

}
