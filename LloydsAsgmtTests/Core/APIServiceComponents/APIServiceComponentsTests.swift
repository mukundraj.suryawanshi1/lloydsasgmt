//
//  APIServiceComponentsTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt
final class APIServiceComponentsTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testMakeUrlRequestBaseUrl() {
        let components = APIServiceComponents.makeUrlRequest(endPoint: APIServiceEndPoint.fetchNekoList)
        XCTAssertNotNil(components)
        XCTAssertNotNil(components?.url?.scheme)
        XCTAssertNotNil(components?.url?.path())
    }
}
