//
//  APIServiceManagerFactoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt
final class APIServiceManagerFactoryTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testAPIManagerFactory() {
        let apiManagerFactory = APIServiceManagerFactory.createManager()
        apiManagerFactory.fetchData(.fetchNekoList, model: NekobesetListDataModel.self) { jsonHandler in
            XCTAssertNotNil(jsonHandler)
        }
    }

}
