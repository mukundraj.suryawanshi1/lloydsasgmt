//
//  NekobsetPresentationMapperTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 31/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class NekobsetPresentationMapperTests: XCTestCase {
   
    let presentationModel = HomePresentationModel(results: [ArtistPresentationModel(artistHref: "https://twitter.com/Aramarufox/", artistName: "Aramarufox", sourceURL:URL(string: "https://twitter.com/Aramarufox/status/1491744680999940101"), url:URL(string: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png"))])
    
    override class func setUp() {
        super.setUp()
    }
    
    func testMapperDomainToPresentationModel(){
        let mapper = NekobsetPresentationMapper()
        let results = mapper.fromDomainToPresentationModel(domainModel: NekobesetListDomainModel(results: [ArtistDomainModel(artistHref: presentationModel.results[0].artistHref, artistName: presentationModel.results[0].artistName, sourceURL: presentationModel.results[0].sourceURL, url: presentationModel.results[0].url)]))
        
        XCTAssertNotNil(results)
        XCTAssertEqual(results?.results[0].artistHref, "https://twitter.com/Aramarufox/")
        XCTAssertEqual(results?.results[0].artistName, "Aramarufox")
        XCTAssertEqual(results?.results[0].sourceURL?.absoluteString, "https://twitter.com/Aramarufox/status/1491744680999940101")
        XCTAssertEqual(results?.results[0].url?.absoluteString, "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png")
    }
    
    func testInvalidMapperDomainToPresentationModel(){
        let mapper = NekobsetPresentationMapper()
        let results = mapper.fromDomainToPresentationModel(domainModel: NekobesetListDomainModel(results: [ArtistDomainModel(artistHref: presentationModel.results[0].artistHref, artistName: presentationModel.results[0].artistName, sourceURL: presentationModel.results[0].sourceURL, url: presentationModel.results[0].url)]))
        XCTAssertNotNil(results)
        XCTAssertNotEqual(results?.results[0].artistHref, nil)
        XCTAssertNotEqual(results?.results[0].artistName, nil)
        XCTAssertNotEqual(results?.results[0].sourceURL, nil)
        XCTAssertNotEqual(results?.results[0].url, nil)
    }
}
