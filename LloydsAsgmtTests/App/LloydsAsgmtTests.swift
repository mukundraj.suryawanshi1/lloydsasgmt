//
//  LloydsAsgmtTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import XCTest
@testable import LloydsAsgmt
import SwiftUI

final class LloydsAsgmtTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testApp() {
        let cv = LloydsAsgmtApp()
        let body = cv.body
        XCTAssertNotNil(body)
    }
}
