//
//  DetailsViewModelTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt

final class DetailsViewModelTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }

    func testDetailsViewEmptyModel() {
        let model = ArtistPresentationModel(artistHref: "", artistName: "", sourceURL: URL(string: ""), url: URL(string: ""))
        let detailsModel = DetailViewModel(eventDetails: model)
        XCTAssertTrue(detailsModel.eventDetails.artistName.isEmpty)
        XCTAssertTrue(detailsModel.eventDetails.artistHref.isEmpty)
        XCTAssertTrue(((detailsModel.eventDetails.url?.absoluteString.isEmpty) == nil))
        XCTAssertTrue(((detailsModel.eventDetails.sourceURL?.absoluteString.isEmpty) == nil))
    }
    
    func testDetailsViewNonEmptyModel() {
        let model = NekoUtilityManager.nekoArtistPresentationModel()
        let detailsModel = DetailViewModel(eventDetails: model)
        XCTAssertTrue(!detailsModel.eventDetails.artistName.isEmpty)
        XCTAssertTrue(!detailsModel.eventDetails.artistHref.isEmpty)
        XCTAssertTrue(((detailsModel.eventDetails.url?.absoluteString.isEmpty) != nil))
        XCTAssertTrue(((detailsModel.eventDetails.sourceURL?.absoluteString.isEmpty) != nil))
    }
}
