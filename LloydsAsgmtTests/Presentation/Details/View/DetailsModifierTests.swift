//
//  DetailsModifierTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
import SwiftUI
@testable import LloydsAsgmt

final class DetailsModifierTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }
    
    func testAllModifer() {
        let artistNameModifer = Text("Aramarufox")
                                .modifier(ArtistDetailsNameModifier())
        XCTAssertNotNil(artistNameModifer)
    }

}
