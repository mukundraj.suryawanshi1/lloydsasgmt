//
//  DetailsContentViewTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//
import SwiftUI
import XCTest
@testable import LloydsAsgmt

final class DetailsListContentViewTests: XCTestCase {
    
    var model = DetailViewModel(eventDetails: NekoUtilityManager.nekoArtistPresentationModel())
    
    override class func setUp() {
        super.setUp()
    }
    
    func testEventDetails() {
        let cv = DetailsListContentView(event: self.model)
        let body = cv.body
        XCTAssertNotNil(body)
        XCTAssertEqual(model.eventDetails.url?.description, "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png")
        XCTAssertEqual(model.eventDetails.artistName, "Aramarufox")
        XCTAssertEqual(model.eventDetails.artistHref, "https://twitter.com/Aramarufox/")
        XCTAssertEqual(model.eventDetails.sourceURL?.description, "https://twitter.com/Aramarufox/status/1491744680999940101")

    }
    
    func testEventNotEmpty() {
        let event = DetailsListContentView(event: self.model)
        XCTAssertNotNil(event)
    }
}
