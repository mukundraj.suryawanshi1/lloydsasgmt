//
//  HomeViewModelTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class HomeViewModelTests: XCTestCase {

    let viewModelFactory = HomeViewModelFactory.createViewModel()
    
    override class func setUp() {
        super.setUp()
    }

    func testViewModelEmptyData() {
        viewModelFactory.listOfData = HomePresentationModel(results: [])
        XCTAssertTrue(((viewModelFactory.listOfData?.results.count) == 0))
    }
    
    func testViewModelNonEmptyData() {
   
        viewModelFactory.getListModel()
        XCTAssertNotNil(viewModelFactory.getListModel())
   
        viewModelFactory.listOfData = HomePresentationModel(results: [NekoUtilityManager.nekoArtistPresentationModel()])
        XCTAssertTrue(((viewModelFactory.listOfData?.results.count) != 0))
    }

}
