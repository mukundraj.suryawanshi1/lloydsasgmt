//
//  HomeViewModelFactoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt

final class HomeViewModelFactoryTests: XCTestCase {
    let factoryModel = HomeViewModelFactory.createViewModel()

    override class func setUp() {
        super.setUp()
    }
    
    func testCreateModel() {
        XCTAssertNotNil(factoryModel)
    }
    
    func testEmptyListData() {
        factoryModel.listOfData = HomePresentationModel(results: [])
        XCTAssertTrue(((factoryModel.listOfData?.results.count) == 0))
    }
    
    func testGetListData() {
        factoryModel.getListModel()
        XCTAssertNotNil(factoryModel.getListModel())
    }
    
    func testHomeViewModelWithFactoryData() {
        factoryModel.listOfData = HomePresentationModel(results: [NekoUtilityManager.nekoArtistPresentationModel()])
        XCTAssertTrue(((factoryModel.listOfData?.results.count) != 0))
    }

}
