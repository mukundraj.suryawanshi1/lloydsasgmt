//
//  HomeContentViewTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class HomeContentViewTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }

    func testListEmptyData(){
        let emptyData = NekobesetListDataModel(results: [])
        XCTAssertTrue(emptyData.results.isEmpty)
    }
    
    func testArtistDomainlistData() {
        let listData = NekoUtilityManager.nekoArtistDomainModel()
        XCTAssertNotNil(listData)
    }
    
    func testArtistListDataModelData() {
        let listData = NekoUtilityManager.nekoListDataModel()
        XCTAssertNotNil(listData)
    }

    func testMockListData() {
        let listData = NekoUtilityManager.nekoArtistPresentationModel()
        XCTAssertEqual(listData.artistName, NekoUtilityManager.nekoArtistDomainModel().artistName)
        XCTAssertEqual(listData.artistHref, NekoUtilityManager.nekoArtistDomainModel().artistHref)
        XCTAssertEqual(URL(string: listData.url?.absoluteString ?? ""), NekoUtilityManager.nekoArtistDomainModel().url)
        XCTAssertEqual(URL(string: listData.sourceURL?.absoluteString ?? ""), NekoUtilityManager.nekoArtistDomainModel().sourceURL)
    }

    func testMockListPresentationData() {
        let listData = NekoUtilityManager.nekoArtistPresentationModel()
        let listImagePath = String().imagePath(imgUrl: listData.url?.absoluteString)
        let listImageSourcePath = String().imagePath(imgUrl: listData.sourceURL?.absoluteString)
        XCTAssertNotNil(listData)
        XCTAssertNotNil(listImagePath)
        XCTAssertNotNil(listImageSourcePath)
    }
}
