//
//  HomeViewModelTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import XCTest
@testable import LloydsAsgmt

final class HomeViewModelTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }

    func testViewModelData(){
        let viewModelFactory = HomeViewModelFactory.createViewModel()
        viewModelFactory.getListModel()
         XCTAssertNotNil(viewModelFactory)
    }
}
