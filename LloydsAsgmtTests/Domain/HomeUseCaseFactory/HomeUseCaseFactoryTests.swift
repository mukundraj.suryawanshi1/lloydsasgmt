//
//  HomeUseCaseFactoryTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import XCTest
@testable import LloydsAsgmt
final class HomeUseCaseFactoryTests: XCTestCase {

    override class func setUp() {
        super.setUp()
    }

    func testFetchNekoList() {
        let usecaseFactory = HomeUseCaseFactory.createHomeUseCase()
        usecaseFactory.fechNekobesetList { jsonhandler in
            XCTAssertNotNil(jsonhandler)
        }
    }

}
