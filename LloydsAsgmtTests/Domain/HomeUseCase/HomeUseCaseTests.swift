//
//  HomeUseCaseTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import XCTest
@testable import LloydsAsgmt
final class HomeUseCaseTests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
    }
    
    func testHomeUseCaseData() {
        let repoFactory = NekoListDataRepositoryFactory.createRepository()
        let vc = HomeUseCase(mapper: NekobsetPresentationMapper(), repository: repoFactory)
        vc.fechNekobesetList { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                XCTAssertTrue(!model.results.isEmpty)
            case .failure(let error):
                XCTAssertTrue(!error.localizedDescription.isEmpty)
            }
        }
        
    }
    
    
}
