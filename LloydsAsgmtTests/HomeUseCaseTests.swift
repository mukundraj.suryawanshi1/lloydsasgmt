//
//  HomeUseCaseTests.swift
//  LloydsAsgmtTests
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import XCTest
@testable import LloydsAsgmt
final class HomeUseCaseTests: XCTestCase {
    
    override class func setUp() {
        super.setUp()
    }
    
    func testFetchNekoList(){
        let usecaseFactory = HomeUseCaseFactory.createUseCase()
        usecaseFactory.fechNekobesetList { jsonhandler in
            XCTAssertNotNil(jsonhandler)
        }
    }
    
}
