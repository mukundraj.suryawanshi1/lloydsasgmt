//
//  LloydsAsgmtApp.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import SwiftUI

@main

// MARK: - This is a statting point app to contain Home content view along with body content
struct LloydsAsgmtApp: App {
    // body
    var body: some Scene {
        // WindowGroup is used to return HomeContent View
        WindowGroup {
            //HomeContent view along with parameter factory model
            HomeContentView(homeVM: HomeViewModelFactory.createViewModel())
        }
    }
}

