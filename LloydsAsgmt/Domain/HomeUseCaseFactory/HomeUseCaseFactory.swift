//
//  HomeUseCaseFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
// MARK: - This is an enum to contain homeUseCase factory method
enum HomeUseCaseFactory {
    static func createHomeUseCase() -> NekoListUseCaseProtocol {
        return HomeUseCase(mapper: NekobsetPresentationMapper(),
                           repository: NekoListDataRepositoryFactory.createRepository())
    }
}
