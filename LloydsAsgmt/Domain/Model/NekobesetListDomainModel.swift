//
//  NekobesetListDomainModel.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is a struct to contain nekoList domain model
struct NekobesetListDomainModel {
    var results: [ArtistDomainModel]
}

// MARK: - This is a struct to contain artistDomain model
struct ArtistDomainModel: Hashable {
    let artistHref: String
    let artistName: String
    let sourceURL: URL?
    let url: URL?
}
