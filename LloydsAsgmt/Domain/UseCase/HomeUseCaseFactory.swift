//
//  HomeUseCaseFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
/// - HomeUseCase Factory
enum HomeUseCaseFactory {
    static func createUseCase() -> NekoListUseCaseProtocol{
        return HomeUseCase(mapper: NekobsetPresentationMapper(), repository: RepositoryFactory.createRepository())
    }
}
