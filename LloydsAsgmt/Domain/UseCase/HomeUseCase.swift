//
//  HomeUseCase.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is a class to contain homeUse case method
final class HomeUseCase : NekoListUseCaseProtocol {
    /// - Variable declaration
    private(set) var mapper: NekoListPresentationMapperProtocol
    private(set) var repository: NekoListDataRepositoryProtocol
    
    /// - - Initializes an nekoListPresentationMapperProtocol, nekoListRepositoryProtocol with given parameters.
    /// - Parameters:
    ///   - mapper: take input parameter as mapper
    ///   - repository: take input parameter as respository
    init(mapper: NekoListPresentationMapperProtocol, 
         repository: NekoListDataRepositoryProtocol) {
        self.mapper = mapper
        self.repository = repository
    }
    
    /// - - initiate api calling methods with response handler
    /// - Parameter completion: return response handler along with model and error.
    func fechNekobesetList(_ completion: @escaping (Result<HomePresentationModel, 
                                                    APIError>) -> Void) {
        self.mapper = NekobsetPresentationMapper()
        repository.makeApiCall { jsonModel in
            switch(jsonModel){
            case .success(let model):
                if let mapData = self.mapper.fromDomainToPresentationModel(domainModel: model) {
                    completion(.success(mapData))
                }
            case .failure(let error):
                completion(.failure(APIError.jsonParsingError(error.localizedDescription)))
            }
        }
    }
}
