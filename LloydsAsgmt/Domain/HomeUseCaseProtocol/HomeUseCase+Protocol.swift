//
//  UseCase+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 22/01/24.
//

import Foundation
// MARK: - This is a protocol to contain nekoList useCase method
protocol NekoListUseCaseProtocol{
    func fechNekobesetList(_ completion:@escaping (Result<HomePresentationModel,
                                                   APIError>) -> Void)
}
