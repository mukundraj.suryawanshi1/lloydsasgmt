//
//  APIServiceManagerFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

// MARK: - This is a enum to contain apiService manager factory
enum APIServiceManagerFactory {
    ///  setup apiService manager  method
    /// - Returns: return apiSerivce manager protocol
    static func createManager() -> APIServiceManagerProtocol {
        return APIServiceFactoryHandler.apiser()
    }
}

// MARK: - This is a struct to contain apiService factory handler method
struct APIServiceFactoryHandler {
    /// setup apiService manger
    /// - Returns: return api service manager
    static func apiser() -> APIServiceManager {
        APIServiceManager(apiService: APIService())
    }
}
