//
//  APIServiceComponents.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 05/02/24.
//

import Foundation

protocol apiServiceComponentsProtocol {
    static func makeUrlRequest(endPoint: EndPointType) -> URLRequest?
}

// MARK: - This is a struct to contain apiService components as required data.
struct APIServiceComponents: apiServiceComponentsProtocol {

    /// setup url request method
    /// - Parameter endPoint: take input parameter as endpoint
    /// - Returns: return urlRequest body
    static func makeUrlRequest(endPoint: EndPointType) -> URLRequest? {
        var components = URLComponents()
        components.scheme = endPoint.scheme
        components.host = endPoint.baseURL
        components.path = endPoint.path
        components.queryItems = endPoint.parameters
   
        guard let url = components.url else {
            return nil
        }
        
        let removePercentEncoding = url.absoluteString.removingPercentEncoding
        var urlRequest = URLRequest(url: URL(string: removePercentEncoding!)!)
        urlRequest.httpMethod = endPoint.httpMethod.rawValue
        urlRequest.httpBody = endPoint.data
     
        let headers = endPoint.headers ?? []
        headers.forEach { urlRequest.addValue($0.header.value, forHTTPHeaderField: $0.header.field) }

        return urlRequest
    }
}
