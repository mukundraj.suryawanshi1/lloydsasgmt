//
//  APIServiceManagerFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
/// - APIServiceManager Factory
enum APIServiceManagerFactory {
    static func createManager() -> APIServiceManagerProtocol{
        return APIServiceManager(apiService: APIService())
    }
}
