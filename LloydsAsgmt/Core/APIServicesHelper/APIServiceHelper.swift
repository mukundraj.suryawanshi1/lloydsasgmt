//
//  Service.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is an enum to contain http method request types
enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

// MARK: - This is an enum to contain http header types
enum HTTPHeader {
    case content(String)
    case accept(String)
    case auth(String)
    case custom(String, String)
    
    var header: (field: String, value: String) {
        switch self {
        case .content(let value): return (field: "Content-Type", value: value)
        case .accept(let value): return (field: "Accept", value: value)
        case .auth(let value): return (field: "Authorization", value: "Bearer " + value)
        case .custom(let key, let value): return (field: key, value: value)
        }
    }
}


// MARK: - This is a protocol to contain endPoint data
protocol EndPointType {
    var scheme: String { get }
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var parameters: [URLQueryItem]? { get }
    var headers: [HTTPHeader]? { get }
    var data: Data? { get }
}
