//
//  APIServiceHelperFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
/// - APIServiceHelper Factory
enum APIServiceHelperFactory {
    static func createService() -> NekoListServiceProtocol{
        return NekoListService(apiServiceManager: APIServiceManager(apiService: APIService()))
    }
}
