//
//  APIServicesRouter.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

// MARK: - This is a protocol to contain apiService request protocol method
protocol APIServiceProtocol {
    func request<T:Decodable>(_ endPoint: EndPointType,
                               decode: @escaping (Decodable) -> T?,
                               completion: @escaping (Result<T, APIError>) -> Void)
}

// MARK: - This is a class to contain apiService request and response method
final class APIService<endPoint : EndPointType>: APIServiceProtocol {
    
    ///  Setup request method
    /// - Parameters:
    ///   - endPoint: take input parameter as endPoint type
    ///   - decode:  take input  parameter as model
    ///   - completion:  return response handler along with corresponding model and error.
    func request<T:Decodable>(_ endPoint: EndPointType,
                              decode: @escaping (Decodable) -> T?,
                              completion:@escaping (Result<T, APIError>) -> Void) {

        guard let urlRequest = APIServiceComponents.makeUrlRequest(endPoint: endPoint) else {
            return completion(.failure(APIError.invalidUrl))
        }
         
        parsingJsonResponse(baseUrl: urlRequest,
                            decodingType: T.self,
                            onCompletion: { jsonHandler in
            switch(jsonHandler){
            case .success(let decoder):
                completion(.success(decoder))
            case .failure(let error):
                completion(.failure(APIError.jsonParsingError(error.localizedDescription)))
            }
        })
    }
    
    /// Setup  api Response
    /// - Parameters:
    ///   - baseUrl: take input parameter as baseUrl
    ///   - decodingType: take input parameter as model type
    ///   - onCompletion: return response handler along with model and error.
    private func parsingJsonResponse<T: Decodable>(baseUrl: URLRequest,
                                                   decodingType: T.Type,
                                                   onCompletion: @escaping(Result<T, Error>) -> Void) {
        URLSession.shared.dataTask(with: baseUrl) { jsonData, jsonResponse, jsonError in
            guard let json = jsonData , jsonError == nil else {
                onCompletion(.failure(APIError.invalidJsonResponse))
                return
            }
            do{
                //Check T type is Image or Model based on that met conditions.
                if(T.self == Data.self){
                    if let bindJsonData = jsonData as? T {
                        onCompletion(.success(bindJsonData))
                    }
                }
                else{
                    let jsonModel = try JSONDecoder.init().decode(T.self, from: json)
                    onCompletion(.success(jsonModel))
                }
            }
            catch{
                onCompletion(.failure(APIError.invalidJsonData))
            }
        }.resume()
    }
    
    ///  Setup API request with imageUrl paramater, get response with ImageData.
    /// - Parameters:
    ///   - url: take input parameter as imageUrl
    ///   - onCompletion: return response handler with image data.
    func imageDownload(url: String, onCompletion: @escaping (Data?) -> Void) {
        guard let imgeUrl = URL(string: url) else{
            return
        }
        
        self.parsingJsonResponse(baseUrl: URLRequest(url: imgeUrl), 
                                 decodingType: Data.self) { jsonHandler in
            switch(jsonHandler){
            case .success(let imgData):
                onCompletion(imgData)
            case .failure(_):
                onCompletion(nil)
            }
        }
    }
}
