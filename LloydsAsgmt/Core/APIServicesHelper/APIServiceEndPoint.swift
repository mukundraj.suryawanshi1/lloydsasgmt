//
//  Service+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation
import UIKit

// MARK: - This is an enum to contain apiService end point types
enum APIServiceEndPoint {
    case fetchNekoList
}

// MARK: - This is an extension to contain apiService end point all types as required to make url request.
extension APIServiceEndPoint : EndPointType {
    /// scheme
    var scheme: String { "https" }
    
    /// base url
    var baseURL: String { APIConstants.baseURL.description }
    
    /// middle path
    private var middlePath : String {
        APIConstants.fetchNekoListUrl.description
    }
    
    /// path
    var path: String {
        switch(self){
        case .fetchNekoList:
            return middlePath
        }
    }
    
    /// httpmethod
    var httpMethod: HTTPMethod {
        switch(self){
        case .fetchNekoList:
            return .get
        }
    }
    
    /// parameters
    var parameters: [URLQueryItem]? {
        nil
    }
    
    /// headers
    var headers: [HTTPHeader]? {
        return [HTTPHeader.content("application/json")]
    }
    
    /// data
    var data: Data? {
        switch(self)
        {
            case .fetchNekoList:
            return nil
        }
    }
}
