//
//  APIServicesManager.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation
import UIKit
import SwiftUI

// MARK: This is an enum to contain apiError handler all type cases.
enum APIError : Error {
    case invalidUrl
    case invalidJsonResponse
    case invalidJsonData
    case jsonParsingError(String)
}

// MARK: - This is a protocol to contain apiService manager protocol.
protocol APIServiceManagerProtocol {
    func fetchData<T: Decodable>(_ request: APIServiceEndPoint,
                                 model: T.Type, completion:@escaping(Result<T,APIError>) -> Void)
}

// MARK: - This is a class to contain apiService manager method.
final class APIServiceManager {
    
    /// Variable declared
    private(set) var apiService: APIService<APIServiceEndPoint>
    
    ///  Initializes an api service manager
    /// - Parameter apiService: take input parameter as apiService
    init(apiService: APIService<APIServiceEndPoint>) {
        self.apiService = apiService
    }
}

// MARK: - This is an extension to contain for apiServices manager method implementation.
extension APIServiceManager : APIServiceManagerProtocol {
    /// Implmentation fetch data methods.
    /// - Parameters:
    ///   - request: take input parameter as endpoint, model with response handler.
    ///   - model: take input paramter as type of model.
    ///   - completion: return response handler.
    func fetchData<T : Decodable>(_ request: APIServiceEndPoint,
                                  model: T.Type,
                                  completion:@escaping(Result<T, APIError>) -> Void) {
        apiService.request(request, decode: { json in
            guard let results = json as? T else {return nil}
            return results
        },
         completion: completion)
    }
}

