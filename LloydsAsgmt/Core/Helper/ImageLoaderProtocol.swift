//
//  ImageLoaderProtocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 05/02/24.
//

import Foundation
// MARK: - This is a protocol to contain imageLoader protocol methods
protocol ImageLoaderProtocol {
    var apiServices:  APIService<APIServiceEndPoint>  {get set}
    func loadImage()
}

