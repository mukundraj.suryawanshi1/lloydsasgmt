//
//  ImageLoader.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import Foundation
import UIKit
import SwiftUI

// MARK: - This is a class to contain imageLoader methods
final class ImageLoader: ObservableObject {
    
    /// - - Variable declared
    @Published var image: UIImage?
    private(set) var url: String
    var apiServices : APIService<APIServiceEndPoint>

    init(url: String) {
        self.url = url
        self.apiServices = APIService<APIServiceEndPoint>()
        loadImage()
    }
}

// MARK: - This is an extension to contain image loader methods
extension ImageLoader : ImageLoaderProtocol {
    /// - - Download image via remote url method
    func loadImage() {
        if let cachedImage = ImageCache.get(forKey: url) {
            self.image = cachedImage
            return
        }
        apiServices.imageDownload(url: url) { imgData in
            DispatchQueue.main.async {
                if let imageData = imgData {
                    let image = UIImage(data: imageData)
                    self.image = image
                    if let bindImg = image {
                        ImageCache.set(bindImg, forKey: self.url)
                    }
                }
            }
        }
    }
}

// MARK: - This is a struct to contain imageCache methods
struct ImageCache {
    /// - - Variable declared
     static var cache = NSCache<NSString, UIImage>()

    /// - - Add and fetch method after storing into cache.
    static func set(_ image: UIImage, 
                    forKey key: String) {
        cache.setObject(image, forKey: key as NSString)
    }

    static func get(forKey key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }
}
