//
//  NekoImageView.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import SwiftUI

struct NekoRemoteImageView: View {
    /// - Variable declared
    @ObservedObject var imageLoader: ImageLoader
    @State var imageWidth : CGFloat
    @State var imageHeight : CGFloat
    
    /// - - Initializes an imageUrl, imageWidth and imageHeight with the given parameters
    init(url: String, imageWidth: CGFloat, imageHeight: CGFloat) {
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.imageLoader = ImageLoader(url: url)
    }
    
    /// - - Custom image view
    var body: some View {
        setupImage()
    }
}

/// - - Preview provider method
#Preview {
    NekoRemoteImageView(url:MockData().samples.url?.absoluteString ?? "" , imageWidth: 200, imageHeight: 200)
}
