//
//  ImageExtensions.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 29/01/24.
//

import Foundation
import SwiftUI

/// - Setup image
extension NekoRemoteImageView {
    @ViewBuilder
    func setupImage() -> some View{
        if let image = imageLoader.image {
            Image(uiImage: image)
                .imageModifier(imgWidth: imageWidth, imgHeight: imageHeight)
            
        } else {
            Image(uiImage: UIImage(named: ImagePlaceHolder.defaultImage.description)!)
                .imageModifier(imgWidth: imageWidth, imgHeight: imageHeight)
        }
    }
}

extension Image {
    /// - - initialze an imageWidth, imgHeight with given parameters.
    func imageModifier(imgWidth: CGFloat,imgHeight : CGFloat) -> some View{
        self
            .resizable()
            .scaledToFit()
            .cornerRadius(18.0)
            .frame(width: imgWidth,height: imgHeight)
            .clipShape(.rect)
    }
}

extension ImageLoader : ImageLoaderProtocol{
    /// - - Download image via remote url method
    func loadImage() {
        if let cachedImage = ImageCache.get(forKey: url) {
            self.image = cachedImage
            return
        }
        apiServices.imageDownload(url: url) { imgData in
            DispatchQueue.main.async {
                if let imageData = imgData {
                    let image = UIImage(data: imageData)
                    self.image = image
                    ImageCache.set(image!, forKey: self.url)
                }
            }
        }
    }
}
