//
//  Constants.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation
import SwiftUI

/// - - APIConstants values
enum APIConstants{
    case baseURL
    case fetchNekoListUrl
    
    var description : String {
        switch(self){
        case .baseURL:
            return "nekos.best"
        case .fetchNekoListUrl:
            return "/api/v2/search?query=Aramaru&type=1&amount=100"
        }
    }
}

/// - - Navigation title values
enum NavTitleConstants{
    case homeNavTitle
    
    var description : String{
        switch(self){
        case .homeNavTitle:
            return "Nekobeset"
        }
    }
}

/// - - Image placeholder
enum ImagePlaceHolder{
    case defaultImage
    
    var description : String {
        switch(self){
        case .defaultImage :
            return "placeholder"
        }
    }
}

/// - - Colors constants values.
enum Colors {
    case backgroundViewColor
    
    var bgColor : Color {
        switch(self){
        case .backgroundViewColor :
            return Color.teal
        }
    }
}

