//
//  APPConstants.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 05/02/24.
//

import Foundation
import SwiftUI

// MARK: - This is an enum to contain navigation title values
enum NavTitleConstants {
    case homeNavTitle
    case alertButtonTitle
    
    /// return values
    var description : String {
        switch(self) {
        case .homeNavTitle:
            return "Nekobeset"
        case .alertButtonTitle:
            return "Alert!"
        }
    }
}

// MARK: - This is an enum to contain image placeholder values
enum ImagePlaceHolder {
    case defaultImage
    
    /// return values
    var description : String {
        switch(self) {
        case .defaultImage :
            return "placeholder"
        }
    }
}

// MARK: - This is an enum to contain color values.
enum Colors {
    case backgroundViewColor
    case navigationAccentColor
    
    /// return values
    var bgColor : Color {
        switch(self) {
        case .backgroundViewColor :
            return Color.teal
        case .navigationAccentColor:
            return Color.white
        }
    }
}

// MARK: - This is an enum to contain container view as mapped different types of views
enum ContainerView {
    case HomeContentView
    case DetailsListContentView
}

// MARK: - This is an enum to contain image size width and height values
enum ImageSize {
    /// Setup image size method
    /// - Parameter viewType: take input parameter as viewType
    /// - Returns: return width and height based on view type
    static func nekoImageSize(viewType : ContainerView) -> CGSize {
        switch(viewType) {
        case .HomeContentView:
            return CGSize(width: 180, height: 180)
        case .DetailsListContentView:
            return CGSize(width: 350, height: 350)
        }
    }
}

// MARK: - This is an enum to contain details view vstack and padding values.
enum DetailsViewSize {
    case VStackSpacing
    case topPadding
    case ImageCornerRadius
    
    /// return values
    var values : CGFloat {
        switch(self){
        case .VStackSpacing:
            return 20
        case .topPadding:
            return 100
        case .ImageCornerRadius:
            return 18.0
        }
    }
}
