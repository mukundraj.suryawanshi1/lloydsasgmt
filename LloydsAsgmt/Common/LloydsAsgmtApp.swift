//
//  LloydsAsgmtApp.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import SwiftUI

@main
struct LloydsAsgmtApp: App {
    var body: some Scene {
        WindowGroup {
            HomeContentView(homeVM: HomeViewModelFactory.createViewModel())
        }
    }
}

