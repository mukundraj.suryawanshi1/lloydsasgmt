//
//  CommonExtensions.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 06/02/24.
//

import Foundation
import SwiftUI
import UIKit

// MARK: - This is an extenstion to contain image modifier method
extension Image {
    /// - - initialze an imageWidth, imgHeight with given parameters.
    func imageModifier(imgWidth: CGFloat,
                       imgHeight : CGFloat) -> some View {
        self
            .resizable()
            .scaledToFit()
            .cornerRadius(DetailsViewSize.ImageCornerRadius.values)
            .frame(width: imgWidth,height: imgHeight)
            .clipShape(.rect)
    }
}

// MARK: - This is an extenstion to contain string method
extension String{
    /// fetch image path with given image url string
    func imagePath(imgUrl : String?) -> String {
        guard let imageUrl = imgUrl else {
            return String()
        }
        return imageUrl
    }
}

// MARK: - This is an extenstion to contain apiError values
extension APIError {
    /// APIError description details
    var errorDescription: String {
        switch(self){
        case .invalidUrl:
            return "URL isn't valid!"
        case .invalidJsonResponse:
            return "Response data is invalid!"
        case .invalidJsonData:
            return "failed to decode json data!"
        case .jsonParsingError(let error):
            return "Something went wrong..\(error)"
        }
    }
}

