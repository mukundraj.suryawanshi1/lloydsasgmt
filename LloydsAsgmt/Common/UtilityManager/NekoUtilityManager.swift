//
//  NekoUtilityManager.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 07/02/24.
//

import Foundation

// MARK: - This is a struct to contain nekoUtiltiy manager used for unit testing purpose along with hard coded values.
struct NekoUtilityManager {

    /// NekobesetList Data model
    /// - Returns: return nekoListDataModel
    static func nekoListDataModel() -> NekobesetListDataModel {
        return NekobesetListDataModel(results: [ArtistDataModel(artistHref: "https://twitter.com/Aramarufox/",
                                                                artistName: "Aramarufox",
                                                                sourceURL: "https://twitter.com/Aramarufox/status/1491744680999940101",
                                                                url: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png"),
                                                ArtistDataModel(artistHref: "https://www.pixiv.net/en/users/12501110",
                                                                artistName: "maruma",
                                                                sourceURL: "https://www.pixiv.net/en/artworks/78293758",
                                                                url: "https://nekos.best/api/v2/neko/dd9e714a-898d-4a36-bdf1-633303706f04.png"),
                                                ArtistDataModel(artistHref: "https://www.pixiv.net/en/users/12501110",
                                                                artistName: "maruma",
                                                                sourceURL: "https://www.pixiv.net/en/artworks/78293758",
                                                                url: "https://nekos.best/api/v2/neko/9fd4bc89-a093-4087-b9a6-59ff79d978f6.png"),
                                                ArtistDataModel(artistHref: "https://www.pixiv.net/en/users/12501110",
                                                                artistName: "maruma",
                                                                sourceURL: "https://www.pixiv.net/en/artworks/82938575",
                                                                url: "https://nekos.best/api/v2/neko/965cf4ee-dc04-45a5-afe7-cb2609f6a2f6.png"),
                                                ArtistDataModel(artistHref: "https://www.pixiv.net/en/users/24157148",
                                                                artistName: "srm",
                                                                sourceURL: "https://www.pixiv.net/en/artworks/99899553",
                                                                url: "https://nekos.best/api/v2/husbando/d18cb369-9a07-45aa-b213-e8618cb5bc86.png")
        ])
    }
    
    /// nekoArtistPresention model
    /// - Returns: return artistPresentionModel
    static func nekoArtistPresentationModel() -> ArtistPresentationModel {
        return ArtistPresentationModel(artistHref: "https://twitter.com/Aramarufox/",
                                              artistName: "Aramarufox",
                                              sourceURL: URL(string: "https://twitter.com/Aramarufox/status/1491744680999940101"),
                                              url: URL(string: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png"))
    }
    
    /// nekoArtistDomain model
    /// - Returns: return artistDomain model
    static func nekoArtistDomainModel() -> ArtistDomainModel {
        return  ArtistDomainModel(artistHref: "https://twitter.com/Aramarufox/",
                                                artistName: "Aramarufox",
                                                sourceURL: URL(string: "https://twitter.com/Aramarufox/status/1491744680999940101"),
                                                url: URL(string: "https://nekos.best/api/v2/kitsune/1c2b22de-7f40-4b91-b88d-a62d66a2198d.png"))
    }
}
