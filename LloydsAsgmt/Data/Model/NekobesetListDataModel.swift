//
//  NekobesetListDataModel.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is a struct to contain nekobesetList data model
struct NekobesetListDataModel: Codable {
    var results: [ArtistDataModel]
}

// MARK: - This is a struct to contain artistData model
struct ArtistDataModel:Codable {
    let artistHref: String
    let artistName: String
    let sourceURL: String
    let url: String
    enum CodingKeys: String, CodingKey {
        case artistHref = "artist_href"
        case artistName = "artist_name"
        case sourceURL = "source_url"
        case url
    }
}

