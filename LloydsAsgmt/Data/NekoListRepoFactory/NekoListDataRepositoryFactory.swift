//
//  RepositoryFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
// MARK: - This is an enum to contain nekoList data repository factory method
enum NekoListDataRepositoryFactory {
    /// Create repository
    /// - Returns: return nekoList data repository protocol
    static func createRepository() -> NekoListDataRepositoryProtocol {
        return NekoListDataRepository(service: APIServiceHelperFactory.createService(),
                          mapper: NekobsetListMapper())
    }
}
