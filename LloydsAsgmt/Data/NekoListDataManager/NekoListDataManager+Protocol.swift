//
//  NekoListService+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

// MARK: - This is a protocol to contain nekoList data manager method
protocol NekoListDataManagerProtocol {
    func fetchNekoList(_ completion:@escaping (Result<NekobesetListDataModel,APIError>) -> Void)
}
