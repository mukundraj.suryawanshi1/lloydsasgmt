//
//  APIServiceHelperFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

// MARK: - This is an enum to contain apiService helper factory
enum APIServiceHelperFactory {
    /// Create service method
    /// - Returns: return nekoListData manager
    static func createService() -> NekoListDataManager {
        return NekoListDataManager(apiServiceManager: APIServiceFactoryHandler.apiser())
    }
}
 
