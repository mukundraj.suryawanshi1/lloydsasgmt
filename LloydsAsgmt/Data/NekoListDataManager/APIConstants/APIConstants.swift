//
//  Constants.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation
import SwiftUI

// MARK: - This is an enum to contain apiConstants all types case with values
enum APIConstants{
    case baseURL
    case fetchNekoListUrl
    
    var description : String {
        switch(self){
        case .baseURL:
            return "nekos.best"
        case .fetchNekoListUrl:
            return "/api/v2/search?query=Aramaru&type=1&amount=100"
        }
    }
}
