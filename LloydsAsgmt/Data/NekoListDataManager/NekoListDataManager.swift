//
//  NekoListService.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

// MARK: - This is a class to contain nekoList data manager
final class NekoListDataManager: NekoListDataManagerProtocol {
    
    private(set) var apiServiceManager: APIServiceManagerProtocol
    
    /// - - Initialise apiServiceManager
    /// - Parameter apiServiceManager: take input parameter as apiService manager
    init(apiServiceManager: APIServiceManagerProtocol) {
        self.apiServiceManager = apiServiceManager
    }
    
    /// Make api request
    /// - Parameter completion: return response handler along with model and error.
    func fetchNekoList(_ completion: @escaping (Result<NekobesetListDataModel, APIError>) -> Void) {
        self.apiServiceManager.fetchData(.fetchNekoList,
                                         model: NekobesetListDataModel.self) { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                completion(.success(model))
            case .failure(let error):
                completion(.failure(APIError.jsonParsingError(error.localizedDescription)))
            }
        }
    }
}
