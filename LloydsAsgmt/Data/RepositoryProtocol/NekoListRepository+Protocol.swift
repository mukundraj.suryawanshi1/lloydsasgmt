//
//  Repository+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation
import UIKit

// MARK: - This is protocol to contain nekoList data repository method
protocol NekoListDataRepositoryProtocol {
    func makeApiCall(_ completion:@escaping (Result<NekobesetListDomainModel,
                                             APIError>) -> Void)
}
