//
//  Repository.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is a class to contain nekoList data repository method
final class NekoListDataRepository: NekoListDataRepositoryProtocol {
    
    /// - Variable declartion
    private(set) var service: NekoListDataManagerProtocol
    private(set) var mapper: NekoListBaseMappperProtocol
  
    /// - - Initializes an serviceProtocol, nekoListBaseMapper protocol with given parameters.
    /// - Parameters:
    ///   - service: take input parameter as serviceProtocol
    ///   - mapper: take input parameter as mapper
    init(service: NekoListDataManagerProtocol,
         mapper: NekoListBaseMappperProtocol) {
        self.service = service
        self.mapper = mapper
    }
  
    /// - - Make api call methods with response handler
    /// - Parameter completion: return response handler along with model and error
    func makeApiCall(_ completion: @escaping (Result<NekobesetListDomainModel, APIError>) -> Void) {
        service.fetchNekoList { jsonModel in
            switch(jsonModel){
            case .success(let model):
                if let mapData = self.mapper.fromDataToDomainModel(dataModel: model) {
                    completion(.success(mapData))
                }
            case .failure(let error):
                completion(.failure(APIError.jsonParsingError(error.localizedDescription)))
            }
        }
    }
}
