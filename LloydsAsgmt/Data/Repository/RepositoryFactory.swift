//
//  RepositoryFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
/// - Repository factory
enum RepositoryFactory {
    static func createRepository() -> NekoListRepositoryProtocol{
        return Repository(service: APIServiceHelperFactory.createService(), mapper: NekobsetListMapper())
    }
}
