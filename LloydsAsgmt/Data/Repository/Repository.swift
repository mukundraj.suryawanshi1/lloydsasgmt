//
//  Repository.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

/// - Repository class
final class Repository: NekoListRepositoryProtocol {
    
    /// - Variable declartion
    private(set) var service: NekoListServiceProtocol
    private(set) var mapper: NekoListBaseMappperProtocol
  
    /// - - Initializes an serviceProtocol, nekoListBaseMapper protocol with given parameters.
    init(service: NekoListServiceProtocol, mapper: NekoListBaseMappperProtocol) {
        self.service = service
        self.mapper = mapper
    }
  
    /// - - Make api call methods with response handler
    func makeApiCall(_ completion: @escaping (Result<NekobesetListDomainModel, Error>) -> Void) {
        service.fetchNekoList { jsonModel in
            switch(jsonModel){
            case .success(let model):
                if let mapData = self.mapper.fromDataToDomainModel(dataModel: model) {
                    completion(.success(mapData))
                }
            case .failure(let error):
                completion(.failure(APIError.jsonParsingError(error.localizedDescription)))
            }
        }
    }
}
