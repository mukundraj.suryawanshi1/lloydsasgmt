//
//  Mapper+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is a protocol to contain nekoList Base Mappper method
protocol NekoListBaseMappperProtocol {
    func fromDataToDomainModel(dataModel: NekobesetListDataModel) -> NekobesetListDomainModel?
}

