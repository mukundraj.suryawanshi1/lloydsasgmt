//
//  NektobsetListMapper.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation

// MARK: - This is a class NekobsetList mapper class
final class NekobsetListMapper: NekoListBaseMappperProtocol {
    /// - Added dataDomainModel methods
    func fromDataToDomainModel(dataModel: NekobesetListDataModel) -> NekobesetListDomainModel? {
        let results = dataModel.results.map { event in
             ArtistDomainModel(artistHref: event.artistHref,
                               artistName: event.artistName,
                               sourceURL: URL(string: event.sourceURL),
                               url: URL(string: event.url))
        }
        return NekobesetListDomainModel(results: results)
    }
}
