//
//  NekoListService+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

/// - NekoListService protocol
protocol NekoListServiceProtocol {
    func fetchNekoList(_ completion:@escaping (Result<NekobesetListDataModel,Error>) -> Void)
}
