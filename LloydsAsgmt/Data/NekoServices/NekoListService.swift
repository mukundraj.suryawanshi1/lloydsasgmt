//
//  NekoListService.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation

/// - MARK:- Created NekoListService class
final class NekoListService: NekoListServiceProtocol {
   
    private(set) var apiServiceManager: APIServiceManagerProtocol
    
    /// - - Initialise apiServiceManager parameter
    init(apiServiceManager: APIServiceManagerProtocol) {
        self.apiServiceManager = apiServiceManager
    }
    
    /// - make api request with the help of response handler
    func fetchNekoList(_ completion: @escaping (Result<NekobesetListDataModel, Error>) -> Void) {
        self.apiServiceManager.fetchData(.fetchNekoList, model: NekobesetListDataModel.self) { jsonHandler in
            switch(jsonHandler){
            case .success(let model):
                completion(.success(model))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
