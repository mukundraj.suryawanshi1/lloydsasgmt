//
//  ImageExtensions.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 29/01/24.
//

import Foundation
import SwiftUI

// MARK: - This is a extension to contain nekoRemote image view methods
extension NekoRemoteImageView {
    
    @ViewBuilder
    /// Setup image
    /// - Returns: return remote image along with modifer values
    func setupImage() -> some View {
        if let image = imageLoader.image {
            Image(uiImage: image)
                .imageModifier(imgWidth: imageWidth, 
                               imgHeight: imageHeight)
            
        } else {
            Image(uiImage: UIImage(named: ImagePlaceHolder.defaultImage.description)!)
                .imageModifier(imgWidth: imageWidth, 
                               imgHeight: imageHeight)
                .overlay(
                    ProgressView().controlSize(.extraLarge).tint(.white)
                )
        }
    }
}
