//
//  NekoImageView.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import SwiftUI

// MARK: - This is a struct to contain nekoRemotImage view
struct NekoRemoteImageView: View {
    /// Variable declared
    @ObservedObject var imageLoader: ImageLoader
    @State var imageWidth : CGFloat
    @State var imageHeight : CGFloat
    
    /// Initializes an imageUrl, imageWidth and imageHeight with the given parameters
    /// - Parameters:
    ///   - url: take input url parameter as String
    ///   - imageWidth: take input imageWidth as CGFloat
    ///   - imageHeight: take input imageHeight as CGFloat
    init(url: String, imageWidth: CGFloat, imageHeight: CGFloat) {
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.imageLoader = ImageLoader(url: url)
    }
    
    // MARK: - Body view
    var body: some View {
        setupImage()
    }
}

// MARK: - Preview provider method
#Preview {
    NekoRemoteImageView(url:NekoUtilityManager.nekoArtistDomainModel().url?.absoluteString ?? "" ,
                        imageWidth: ImageSize.nekoImageSize(viewType: .HomeContentView).width,
                        imageHeight: ImageSize.nekoImageSize(viewType: .HomeContentView).height)
}
