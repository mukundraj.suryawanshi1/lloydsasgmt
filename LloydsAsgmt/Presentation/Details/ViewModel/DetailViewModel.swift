//
//  DetailViewModel.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import Foundation

// MARK: - This is a protocol to contain details protocol method
protocol DetailsProtocol: AnyObject, ObservableObject {
    var eventDetails: ArtistPresentationModel { get set }
}

// MARK: - This is a class to contain detailView model
final class DetailViewModel: DetailsProtocol {

    /// Variable declared
    var eventDetails: ArtistPresentationModel

    /// Initializes an artistPresentationModel with given parameters.
    /// - Parameter eventDetails: take input parameter as eventDetails of artistPresention model
    init(eventDetails: ArtistPresentationModel) {
        self.eventDetails = eventDetails
    }
}
