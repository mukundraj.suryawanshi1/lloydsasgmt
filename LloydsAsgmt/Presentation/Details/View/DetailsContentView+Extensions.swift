//
//  DetailsContentView+Extensions.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 29/01/24.
//

import Foundation
import SwiftUI

extension DetailsListContentView {
    
    /// - Config details data with UI
    @ViewBuilder
    func setupNavigationView() -> some View {
        NavigationStack{
            ZStack(alignment:.topLeading, content: {
                Colors.backgroundViewColor.bgColor.ignoresSafeArea()
                setupTextDetailsView()
            })
        }.navigationBarTitle(event.eventDetails.artistName.capitalized,displayMode: .inline)
            .toolbarRole(.editor)
    }
    
    /// - Setup right side details view.
    @ViewBuilder
    func setupTextDetailsView() -> some View {
        VStack{
            HStack{
                NekoRemoteImageView(url: event.eventDetails.url!.absoluteString, imageWidth: 250, imageHeight: 250)
                VStack(alignment: .leading, spacing: 20){
                    Text(event.eventDetails.artistName.capitalized)
                        .modifier(ArtistDetailsNameModifier())
                    Text(event.eventDetails.artistHref)
                        .modifier(ArtistDetailsHrefModifier())
                    Text(event.eventDetails.sourceURL!.absoluteString)
                        .modifier(ArtistDetailsSourceUrlModifier())
                }
                Spacer()
            }
        }.padding(.vertical,150)
    }
}

/// - Custom Modifier as ArtistDetailsName Modifier
struct ArtistDetailsNameModifier : ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.headline)
            .fontWeight(.bold)
            .foregroundColor(.white)
    }
}
/// - ArtistDetailsHref Modifier
struct ArtistDetailsHrefModifier : ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.subheadline)
            .fontWeight(.regular)
            .foregroundColor(.white)
    }
}
/// -ArtistDetailsSourceUrl Modifier
struct ArtistDetailsSourceUrlModifier : ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.caption)
            .fontWeight(.light)
            .foregroundColor(.white)
    }
}
