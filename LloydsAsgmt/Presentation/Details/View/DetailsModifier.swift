//
//  DetailsModifier.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 29/01/24.
//

import Foundation
import SwiftUI

// MARK: - This is a struct to contain artistDetailsName modifier
struct ArtistDetailsNameModifier : ViewModifier {
    /// setup custom modifier
    /// - Parameter content: body content
    /// - Returns: return modifer along with font style, color, etc.
    func body(content: Content) -> some View {
        content
            .font(.headline)
            .fontWeight(.semibold)
            .foregroundColor(.white)
    }
}
