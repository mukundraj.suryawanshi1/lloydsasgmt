//
//  DetailsContentView.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import SwiftUI

@MainActor
// MARK: - This is a struct to contain detailsListContent view data.
struct DetailsListContentView<events: DetailsProtocol>: View {
    // Variable declared
    var event : events

    // MARK: - Body view
    var body: some View {
        setupNavigationView()
    }
}

// MARK: - This is a extension to contain setup navigation,setup textDetils methods.
extension DetailsListContentView {
    
    @ViewBuilder
    /// Config details data with UI
    /// - Returns: return navigation stack view.
    func setupNavigationView() -> some View {
        NavigationStack{
            ZStack(alignment:.top, content: {
                Colors.backgroundViewColor.bgColor.ignoresSafeArea()
                setupTextDetailsView()
            })
        }
        .navigationBarTitle(event.eventDetails.artistName.capitalized,
                            displayMode: .large)
        .toolbarRole(.editor)
    }
    
    @ViewBuilder
    /// Setup details view.
    /// - Returns: return text details view data.
    func setupTextDetailsView() -> some View {
        VStack(spacing: DetailsViewSize.VStackSpacing.values, content: {
            let imageUrl = String().imagePath(imgUrl: event.eventDetails.url?.absoluteString)
            NekoRemoteImageView(url: imageUrl,
                                imageWidth: ImageSize.nekoImageSize(viewType: .DetailsListContentView).width,
                                imageHeight: ImageSize.nekoImageSize(viewType: .DetailsListContentView).height)
                                .padding(.top, DetailsViewSize.topPadding.values)
            Text(event.eventDetails.artistName.capitalized)
                .modifier(ArtistDetailsNameModifier())
        })
    }
}

// MARK: - Preview provider method
#Preview {
    DetailsListContentView(event: DetailViewModel(eventDetails: NekoUtilityManager.nekoArtistPresentationModel()))
}
