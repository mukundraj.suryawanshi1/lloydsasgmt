//
//  ArtistPresentationModel.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 23/01/24.
//

import Foundation

// MARK: - This is a struct to contain artistPresention model data.
struct ArtistPresentationModel: Identifiable, Hashable {
    var id = UUID()
    let artistHref: String
    let artistName: String
    let sourceURL: URL?
    let url: URL?
}
