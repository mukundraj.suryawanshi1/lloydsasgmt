//
//  HomePresentationModel.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 22/01/24.
//

import Foundation
// MARK: - This is a struct to contain homePresention model data.
struct HomePresentationModel {
    let results: [ArtistPresentationModel]
}

