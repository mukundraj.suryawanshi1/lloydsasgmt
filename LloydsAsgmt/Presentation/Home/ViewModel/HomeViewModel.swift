//
//  HomeViewModel.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import Foundation
import UIKit
// MARK: - This is a class to contain homeViewModel data
final class HomeViewModel: HomeViewModelProtocol {
    
    // Variable delclearation...
    var useCase: NekoListUseCaseProtocol
    @Published var listOfData: HomePresentationModel?
    @Published var isPresentingErrorAlert = false
    @Published var apiError: String
    
    //  Initializes an nekoListUseCaseProtocol, homePresentationModel with given parameters.
    /// - Parameters:
    ///   - useCase: take input parameter as useCase.
    ///   - listOfData: take input parameter as listData.
    init(useCase: NekoListUseCaseProtocol,
         listOfData: HomePresentationModel? = nil) {
        self.useCase = useCase
        self.listOfData = listOfData
        self.apiError = String()
    }
    
    // get api list data using method
    func getListModel() {
        useCase.fechNekobesetList({ model in
            switch(model){
            case .success(let model):
                DispatchQueue.main.async { [weak self] in
                    self?.isPresentingErrorAlert = false
                    self?.listOfData = model
                }
            case .failure(let error):
                DispatchQueue.main.async { [weak self] in
                    self?.isPresentingErrorAlert = true
                    self?.apiError = error.errorDescription
                }
            }
        })
    }
}


