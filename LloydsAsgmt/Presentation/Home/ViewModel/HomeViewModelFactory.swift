//
//  HomeViewModelFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
/// -HomeViewModel Factory
enum HomeViewModelFactory {
    static func createViewModel() -> HomeViewModel {
        return HomeViewModel(useCase: HomeUseCaseFactory.createUseCase())
    }
}
