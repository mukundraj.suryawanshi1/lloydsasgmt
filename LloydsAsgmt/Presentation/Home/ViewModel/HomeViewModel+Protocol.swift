//
//  HomeViewModel+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 22/01/24.
//

import Foundation
/// -HomeViewModelInterfaces protocol
protocol HomeViewModelInterfaces{
    var useCase : NekoListUseCaseProtocol {get set}
    var listOfData: HomePresentationModel? {get set}
    func getListModel() -> Void
}
