//
//  PresentationMapper.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 22/01/24.
//

import Foundation

// MARK: - This is a protocol to contain nekoListPresentation mapper method
protocol NekoListPresentationMapperProtocol {
    func fromDomainToPresentationModel(domainModel: NekobesetListDomainModel) -> HomePresentationModel?
}
