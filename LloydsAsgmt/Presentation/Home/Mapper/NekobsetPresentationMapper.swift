//
//  NekobsetPresentationMapper.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 22/01/24.
//

import Foundation

// MARK: - This is a class to contain nekobsetPresentMapper methods
final class NekobsetPresentationMapper: NekoListPresentationMapperProtocol {
    
    /// Added dataDomainModel methods
    /// - Parameter domainModel: take input parameter  as domain model
    /// - Returns: return domain data into home presentation model
    func fromDomainToPresentationModel(domainModel: NekobesetListDomainModel) -> HomePresentationModel? {
        let results = domainModel.results.map { event in
            ArtistPresentationModel(artistHref: event.artistHref,
                                    artistName: event.artistName,
                                    sourceURL: event.sourceURL,
                                    url: event.url)
        }
        return HomePresentationModel(results: results)
    }
}
