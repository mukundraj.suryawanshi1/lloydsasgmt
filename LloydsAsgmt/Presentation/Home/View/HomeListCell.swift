//
//  HomeListCell.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 18/01/24.
//

import SwiftUI

// MARK: - This is a struct to contain custom cell configuration along with data
struct HomeListCell: View {
    /// Variable declared artist presentation model
    private var event : ArtistPresentationModel
    
    /// Initializes an artistPresentationModel with given parameters.
    /// - Parameter event: event  of artist presentation model
    init(event: ArtistPresentationModel) {
        self.event = event
    }
    
    // MARK: - Body view
    var body: some View {
        // setup config cell with event parameter
        configCell(event: self.event)
    }
}

// MARK: - This is a extension to contain custom cell as required methods.
extension HomeListCell {
    
    @ViewBuilder
    /// Setup cusom cell with api data.
    /// - Parameter event: take input paramater as event of artistPresentModel
    /// - Returns: cell with image and title
    func configCell(event : ArtistPresentationModel) -> some View {
        HStack(alignment: .center,
               content: {
            let imagePath = String().imagePath(imgUrl: event.url?.absoluteString)
            NekoRemoteImageView(url: imagePath,
                                imageWidth: ImageSize.nekoImageSize(viewType: ContainerView.HomeContentView).width,
                                imageHeight: ImageSize.nekoImageSize(viewType: ContainerView.HomeContentView).height)
            Text(event.artistName)
                .modifier(ArtistNameModifier())
            Spacer()
        })
    }
    
}

// MARK: - Preview provider method
#Preview {
    HomeListCell(event: NekoUtilityManager.nekoArtistPresentationModel())
}


