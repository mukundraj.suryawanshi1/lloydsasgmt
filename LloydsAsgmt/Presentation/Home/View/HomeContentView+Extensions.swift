//
//  HomeContentView+Extensions.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 29/01/24.
//

import Foundation
import SwiftUI

/// - - setup config data with UI
extension HomeContentView{
    
    @ViewBuilder
    func setupNavigationView() -> some View{
        NavigationStack {
            List{
                setupConfigData()
            }.listStyle(.grouped)
                .task {
                    homeVM.getListModel()
                }
                .navigationTitle(NavTitleConstants.homeNavTitle.description)
        }
    }
    
    @ViewBuilder
    func setupConfigData() -> some View {
        if let bind = homeVM.listOfData?.results {
            ForEach(bind,id: \.self) { event in
                NavigationLink(destination: DetailsListContentView(event: DetailViewModel(eventDetails: event))) {
                    HomeListCell(event: event)
                }
            }
            .listRowBackground(Colors.backgroundViewColor.bgColor)
        }
    }
}
/// - - Setup cusom cell with api data.
extension HomeListCell {
    @ViewBuilder
    func configCell(event : ArtistPresentationModel) -> some View{
        HStack(alignment: .center, content: {
            NekoRemoteImageView(url: event.url!.absoluteString, imageWidth: 180, imageHeight: 180)
            Text(event.artistName)
                .modifier(ArtistNameModifier())
            Spacer()
        })
    }
    
}

/// - - Custom ArtistNameModifier Modifier
struct ArtistNameModifier : ViewModifier{
    func body(content : Content) -> some View{
        content
            .textCase(.uppercase)
            .font(.subheadline)
            .fontWeight(.medium)
            .foregroundColor(.white)
        Spacer()
    }
}
