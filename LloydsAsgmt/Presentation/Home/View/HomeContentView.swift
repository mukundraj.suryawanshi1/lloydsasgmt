//
//  ContentView.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 17/01/24.
//

import SwiftUI

// MARK: - This is a struct to contain home view pouplate list of nekoArtist data along with images, titles.
struct HomeContentView<homeViewMModel: HomeViewModelProtocol>: View {
    /// Variable declared homeView model class
    @ObservedObject var homeVM: homeViewMModel

    // MARK: - body View
    var body: some View {
        // setup navigation method
        setupNavigationView()
    }
}


// MARK: - This is extension to contain home content view required methods.
extension HomeContentView {
    
    @ViewBuilder
    /// setup navigation view
    /// - Returns: navigation stack view along with List items
    private func setupNavigationView() -> some View {
        NavigationStack {
            List{
                setupConfigData()
            }.listStyle(.grouped)
                .task {
                    homeVM.getListModel()
                }
                .alert(isPresented: $homeVM.isPresentingErrorAlert, 
                       content: {
                    setupErrorAlertMessage()
                })
                .navigationTitle(NavTitleConstants.homeNavTitle.description)
        }.accentColor(Colors.navigationAccentColor.bgColor)
    }
    
    @ViewBuilder
    /// setup config data with UI
    /// - Returns: List of Article items with image, titles.
    private func setupConfigData() -> some View {
        if let bind = homeVM.listOfData?.results {
            ForEach(bind,id: \.self) { event in
                destinaTion(item: event)
            }
            .listRowBackground(Colors.backgroundViewColor.bgColor)
        }
    }
    
    @ViewBuilder
    /// Navigation destination method as DetailsListContentView
    /// - Parameter item: take input parameter as item of artistPresentionModel
    /// - Returns: Custom configuration cell
    private func destinaTion(item: ArtistPresentationModel) -> some View {
        NavigationLink {
            DetailsListContentView(event: DetailViewModel(eventDetails: item))
        } label: {
            HomeListCell(event: item)
        }
    }
    
    /// setup error alert msg while network off mode.
    /// - Returns: Alert pop up along with title , error msg and cancel button.
    private func setupErrorAlertMessage() -> Alert{
        Alert(title: Text(NavTitleConstants.alertButtonTitle.description),message: Text(homeVM.apiError),
              dismissButton: .cancel())
    }
}

// MARK: - Preview provider method
#Preview {
    HomeContentView(homeVM:HomeViewModelFactory.createViewModel())
}
