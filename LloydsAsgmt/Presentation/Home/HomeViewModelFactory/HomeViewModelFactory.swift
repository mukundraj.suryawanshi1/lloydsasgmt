//
//  HomeViewModelFactory.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 30/01/24.
//

import Foundation
// MARK: - This is a enum to contain homeView model factory created
enum HomeViewModelFactory {
    static func createViewModel() -> HomeViewModel {
        return HomeViewModel(useCase: HomeUseCaseFactory.createHomeUseCase())
    }
}
