//
//  HomeViewModel+Protocol.swift
//  LloydsAsgmt
//
//  Created by Mukundraj Suryawanshi on 22/01/24.
//

import Foundation
// MARK: - This is a protocol to contain homeViewModel protocol data
protocol HomeViewModelProtocol: AnyObject, ObservableObject {
    var useCase : NekoListUseCaseProtocol {get set}
    var listOfData: HomePresentationModel? {get set}
    func getListModel() -> Void
    var isPresentingErrorAlert : Bool {get set}
    var apiError : String {get set}
}
